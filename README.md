// URL shortener version 1.0
// Author: Stephen Phillips
// Created On 28/11/20

Introduction
============
A simple PHP class based URL shortener

A user can paste in a URL and have a shortened URL returned
A user can go to go to the short URL and it will redirect to my long URL
A user can track how many clicks my URL has had

Installation
============
Copy zip folder to folder on webserver where you would like to install and extract, then updated the config.inc.php file in /includes/ to set your mySQL database details and also to set the base url to be used as well.
You will need to make sure the .htaccess file in the root folder is also setup to correctly point to the right folders too.
Finally you will need to import the SQL from the db folder into your database as well.

Demo
====
URL Shorten Form
https://stephenphillips.co.uk/shorten/
Redirect to a shortened URL
https://stephenphillips.co.uk/shorten/9ae757
View Stats for a shortened URL
https://www.stephenphillips.co.uk/shorten/stats/9ae757


