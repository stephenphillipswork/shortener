<?php
// URL shortener version 1.0
// Author: Stephen Phillips
// Created On 28/11/20
require('./includes/config.inc.php');
require('./class/shorten.class.php');
$shorten = new Shorten();

/* handle redirect */
if(isset($_GET['redirect']) && $_GET['redirect']!="")
{ 
    $slug=urldecode($_GET['redirect']);
 
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $url= $shorten->getRedirectUrl($slug);
    $conn->close();
    header("location:".$url);
    exit;
}
/* handle output */
include('./views/header.php');
/* stats */
if(isset($_GET['stats']) && $_GET['stats']!="")
{ 
    $slug=urldecode($_GET['stats']);
 
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $hits= $shorten->getUrlStats($slug);
    $conn->close();
    include('./views/stats.php');
}
else{
    /* user input */
    if(isset($_GET['url']) && $_GET['url']!="")
    { 
        // check if we have a valid URL to shorten
        $url=urldecode($_GET['url']);
        if (filter_var($url, FILTER_VALIDATE_URL)) 
        {
            // all good so lets store this URL and show a shortened version
            $conn = new mysqli($servername, $username, $password, $dbname);
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            } 
            $slug=$shorten->getShortUrl($url);
            $conn->close();
    
            include('./views/url.php');
        } 
        else 
        {
            die("$url is not a valid URL");
        }
     
    }
    else
    {	
        // show a form to get a url to shorten
        include('./views/shortenForm.php');
    }
}
include('./views/footer.php');
?>