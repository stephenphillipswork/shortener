<center>
    <h1>Your Short Url Is Shown Below</h1>
    <form>
        <p>Your shortened URL is <a href="<?php echo $base_url.$slug; ?>" target="_blank"><?php echo $base_url.$slug; ?></a></p>
        <h2>Please use the form below to copy your short URL</h2>
        <p><input id="shortUrl" style="width:500px" type="url" name="url" value="<?php echo $base_url.$slug; ?>"></p>
        <p>To view stats for this short URL please go to <a href="<?php echo $base_url.'stats/'.$slug; ?>" target="_blank"><?php echo $base_url.'stats/'.$slug; ?></a></p>
    </form>
</center>    
        