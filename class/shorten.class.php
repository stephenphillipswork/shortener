<?php
// class shorten contains all the functions related to url shortening
// Author: Stephen Phillips
// Created On 28/11/20
class Shorten 
{
    /* public functions */
    // getShortURL
    // Input str URL
    // Description: Adds a shortened URL to the DB, and makes sure it's not already created.
    public function getShortUrl($url){
        global $conn;
        $query = "SELECT * FROM url_shorten WHERE url = '".$url."' "; 
        $result = $conn->query($query);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            return $row['short_code'];
        } else {
            $short_code = $this->generateUniqueID();
            $sql = "INSERT INTO url_shorten (url, short_code, hits)
            VALUES ('".$url."', '".$short_code."', '0')";
            if ($conn->query($sql) === TRUE) {
                return $short_code;
            } else { 
                die("Unknown Error Occured");
            }
        }
    }
    // getRedirectUrl
    // Inputs str slug
    // Description: Takes a shortened URL string and finds the matching URL to redirect to for it
    public function getRedirectUrl($slug){
        global $conn;
        $query = "SELECT * FROM url_shorten WHERE short_code = '".addslashes($slug)."' "; 
        $result = $conn->query($query);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            $hits=$row['hits']+1;
            $sql = "update url_shorten set hits='".$hits."' where id='".$row['id']."' ";
            $conn->query($sql);
            return $row['url'];
        }
        else 
        { 
            die("Invalid Link!");
        }
    }
    // getUrlStats
    // Inputs str slug
    // Description: Takes a shortened URL string and finds the matching URL to redirect to for it
    public function getUrlStats($slug){
        global $conn;
        $query = "SELECT * FROM url_shorten WHERE short_code = '".addslashes($slug)."' "; 
        $result = $conn->query($query);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            return $row['hits'];
        }
        else 
        { 
            die("Invalid Link!");
        }
    }    
    // generateUniqueID
    // Description: generates a unique token to use in shortened URL's
    function generateUniqueID(){
        global $conn; 
        $token = substr(md5(uniqid(rand(), true)),0,6); $query = "SELECT * FROM url_shorten WHERE short_code = '".$token."' ";
        $result = $conn->query($query); 
        if ($result->num_rows > 0) {
            $this->generateUniqueID();
        } else {
            return $token;
        }
    }

}
?>
